#include <GL/glut.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"

#define BUF 1000000
#define PORT 27001
#define MAX_CONN_QUEUE 10

void InviaTCP(PacketHeader* packet, int sock){
	char buf[BUF];
    int tot,sended=0,ret;
    tot = Packet_serialize(buf,packet);
    while(sended<tot){
        ret = send(sock,buf+sended,BUF,0);
        if(ret<0){
            fprintf(stderr,"CLIENT:Errore nell'invio del pacchetto\n");
            return;
        }
        sended += ret;
    }
    return;
}

PacketHeader* RiceviPacchettoTCP(int sock){
	char buf[1000000];
	int ret;
  ret = recv(sock,buf,BUF,MSG_WAITALL);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione TCP\n");
    return NULL;
  }
  return Packet_deserialize(buf,ret);
}  

int main(){
  int sockTCP;
  sockTCP = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  if(sockTCP<0){
    printf("asdSAD\n");
    return 1;
  }
  struct sockaddr_in addrTCP;
  addrTCP.sin_family = AF_INET;
  addrTCP.sin_addr.s_addr = inet_addr("127.0.0.1");
  addrTCP.sin_port = htons(PORT);
  
  if(bind(sockTCP,(struct sockaddr*)&addrTCP,sizeof(struct sockaddr_in))<0){
    printf("asdsadasda\n");
    return 1;
  }
  
  if(listen(sockTCP,MAX_CONN_QUEUE)<0){
    printf("asdasd\n");
    return 1;
  }
  int client_sock;
  struct sockaddr_in client_addr;
  int addr_len = sizeof(struct sockaddr_in);
  client_sock = accept(sockTCP,(struct sockaddr*)&client_addr,(socklen_t*)&addr_len);
  if(client_sock<0){
    printf("asdasd\n");
    return 1;
  }
  printf("Connddesso\n");
  PacketHeader* pack;
  pack = RiceviPacchettoTCP(client_sock);
  printf("RicievutoPaccjettasdsadao\n");
  IdPacket* id = (IdPacket*)pack;
  printf("ricevuto id:%d\n",id->id);
  id->id--;
  InviaTCP((PacketHeader*)id,client_sock);
  
  PacketHeader head;
  IdPacket* idd =(IdPacket*)malloc(sizeof(IdPacket));
  head.type = GetId;
  head.size = sizeof(IdPacket);
  idd->header = head;
  idd->id = 50;
  InviaTCP((PacketHeader*)idd,client_sock);
  printf("Inviato Pacchetto id:%d\n",idd->id);
  pack = RiceviPacchettoTCP(client_sock);
  idd = (IdPacket*)pack;
  printf("Ricevuto Pacchetto:%d\n",idd->id);
  
  
  
  return 0;
}
